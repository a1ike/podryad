jQuery(function ($) {
  $('.phone').inputmask({
    mask: '+7(999)-999-99-99',
    showMaskOnHover: false,
  });

  window.onscroll = function () {
    scrollFunction();
  };

  function scrollFunction() {
    if (
      document.body.scrollTop > 200 ||
      document.documentElement.scrollTop > 200
    ) {
      $('.p-header').addClass('p-header_fixed');
      $('.p-lk__tabs').addClass('p-lk__tabs_fixed');
    } else {
      $('.p-header').removeClass('p-header_fixed');
      $('.p-lk__tabs').removeClass('p-lk__tabs_fixed');
    }
    /* if (
      document.body.scrollTop > 500 ||
      document.documentElement.scrollTop > 500
    ) {
    } else {
    } */
  }

  $('.p-header__mob').on('click', function (e) {
    e.preventDefault();

    $('.p-header__nav').slideToggle('fast');
    $('.p-header__link').slideToggle('fast');
    $('.p-top').slideToggle('fast');
  });

  $('.open-modal').on('click', function (e) {
    e.preventDefault();

    $('.p-modal').toggle();
  });

  $('.p-modal__centered').on('click', function (e) {
    e.preventDefault();

    if (e.target.className === 'p-modal__centered') {
      $('.p-modal').hide();
    }
  });

  $('.p-modal__close').on('click', function (e) {
    e.preventDefault();
    $('.p-modal').hide();
  });

  $('.p-tabs__header li').click(function () {
    var tab_id = $(this).attr('data-tab');

    $('.p-tabs__header li').removeClass('current');
    $('.p-tabs__content').removeClass('current');

    $(this).addClass('current');
    $('#' + tab_id).addClass('current');
  });

  $('.p-home-new__tabs li').click(function () {
    var tab_id = $(this).attr('data-tab');

    $('.p-home-new__tabs li').removeClass('current');

    $(this).addClass('current');
  });

  $('.p-object-view-card').on('mouseenter', function (e) {
    e.preventDefault();

    var link_id = $(this).attr('data-link');

    $('.p-object-view-card').removeClass('current');
    $('.p-object-view__number').removeClass('current');

    $(this).addClass('current');
    $('#' + link_id).addClass('current');
  });

  new Swiper('.p-home-top', {
    /* direction: "vertical", */
    pagination: {
      el: '.p-home-top .swiper-pagination',
      clickable: true,
    },
    navigation: {
      nextEl: '.p-home-top .swiper-button-next',
      prevEl: '.p-home-top .swiper-button-prev',
    },
    loop: true,
  });

  new Swiper('.p-home-new__cards', {
    mousewheel: true,
    scrollbar: {
      el: '.p-home-new .swiper-scrollbar',
      hide: false,
      draggable: true,
    },
    navigation: {
      nextEl: '.p-home-new .swiper-button-next',
      prevEl: '.p-home-new .swiper-button-prev',
    },
    spaceBetween: 30,
    slidesPerView: 'auto',
    loop: true,
    breakpoints: {
      768: {
        spaceBetween: 2,
      },
    },
  });

  new Swiper('.p-home-reviews__cards', {
    mousewheel: true,
    scrollbar: {
      el: '.p-home-reviews .swiper-scrollbar',
      draggable: true,
    },
    navigation: {
      nextEl: '.p-home-reviews .swiper-button-next',
      prevEl: '.p-home-reviews .swiper-button-prev',
    },
    spaceBetween: 30,
    slidesPerView: 1,
    breakpoints: {
      768: {
        slidesPerView: 2,
      },
      1024: {
        slidesPerView: 4,
      },
      1200: {
        slidesPerView: 6,
      },
    },
  });

  new Swiper('.p-wins__cards', {
    mousewheel: true,
    scrollbar: {
      el: '.p-wins .swiper-scrollbar',
      draggable: true,
    },
    navigation: {
      nextEl: '.p-wins .swiper-button-next',
      prevEl: '.p-wins .swiper-button-prev',
    },
    spaceBetween: 30,
    slidesPerView: 1,
    breakpoints: {
      768: {
        slidesPerView: 2,
      },
      1024: {
        slidesPerView: 3,
      },
      1200: {
        slidesPerView: 4,
      },
    },
  });

  new Swiper('.p-photos__cards', {
    mousewheel: true,
    scrollbar: {
      el: '.p-photos .swiper-scrollbar',
      hide: false,
      draggable: true,
    },
    navigation: {
      nextEl: '.p-photos .swiper-button-next',
      prevEl: '.p-photos .swiper-button-prev',
    },
    spaceBetween: 30,
    slidesPerView: 'auto',
    loop: true,
    breakpoints: {
      768: {
        spaceBetween: 2,
      },
    },
  });

  new Swiper('.p-page-news__cards', {
    mousewheel: true,
    scrollbar: {
      el: '.p-page-news .swiper-scrollbar',
      draggable: true,
    },
    navigation: {
      nextEl: '.p-page-news .swiper-button-next',
      prevEl: '.p-page-news .swiper-button-prev',
    },
    spaceBetween: 30,
    slidesPerView: 1,
    breakpoints: {
      1024: {
        slidesPerView: 2,
      },
      1200: {
        slidesPerView: 3,
      },
    },
  });

  $('.p-home-seo__more').on('click', function (e) {
    e.preventDefault();

    if ($(this).hasClass('p-home-seo__more_active')) {
      $(this).removeClass('p-home-seo__more_active');
      $(this).html('Читать дальше');
      $(this).prev().removeClass('p-home-seo__content_active');
    } else {
      $(this).addClass('p-home-seo__more_active');
      $(this).html('Скрыть');
      $(this).prev().addClass('p-home-seo__content_active');
    }
  });

  var lastId,
    topMenu = $('#p-lk__tabs'),
    topMenuHeight = topMenu.outerHeight() + 15,
    // All list items
    menuItems = topMenu.find('a'),
    // Anchors corresponding to menu items
    scrollItems = menuItems.map(function () {
      var item = $($(this).attr('href'));
      if (item.length) {
        return item;
      }
    });

  // Bind click handler to menu items
  // so we can get a fancy scroll animation
  menuItems.click(function (e) {
    var href = $(this).attr('href'),
      offsetTop = href === '#' ? 0 : $(href).offset().top - topMenuHeight + 1;
    $('html, body').stop().animate(
      {
        scrollTop: offsetTop,
      },
      300
    );
    e.preventDefault();
  });

  // Bind to scroll
  $(window).scroll(function () {
    // Get container scroll position
    var fromTop = $(this).scrollTop() + topMenuHeight;

    // Get id of current scroll item
    var cur = scrollItems.map(function () {
      if ($(this).offset().top < fromTop) return this;
    });
    // Get the id of the current element
    cur = cur[cur.length - 1];
    var id = cur && cur.length ? cur[0].id : '';

    if (lastId !== id) {
      lastId = id;
      // Set/remove active class
      menuItems
        .parent()
        .removeClass('active')
        .end()
        .filter('[href=\'#' + id + '\']')
        .parent()
        .addClass('active');
    }
  });

  new SimpleBar($('.p-page-map-popup__cards')[0]);
});
